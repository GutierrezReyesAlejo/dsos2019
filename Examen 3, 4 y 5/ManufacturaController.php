<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\PW\Manufactura;


class ManufacturaController extends Controller{
    public function inicio(){
        return view ('layouts/inicio');
    }

    public function blacklist(Request $request){
        $id_unieco= $request->id_unieco;
        return response()-> json($consulta = manufactura::select('id_unieco')
        ->where('id_unieco','=',$id_unieco)
        ->take(1)->first());
    }

//['id_unieco','nom_estab','raz_social','id_actividad','nom_vial','tipocencom',
//'nom_e','nom_asent','cod_postal','telefono','latitud','longitud'];
    public function insertar_black(Request $request ){
        $id_unieco = $request-> id_unieco;
        $nom_estab = $request-> nom_estab;
        $raz_social = $request -> raz_social;
        $id_actividad = $request-> id_actividad;
        $nom_vial = $request-> nom_vial;
        $tipocencom = $request -> tipocencom;
        $nom_e = $request -> nom_e;
        $nomb_asent = $request -> nomb_asent;
        $cod_postal = $request-> cod_postal;
        $telefono = $request-> telefono;
        $latitud = $request -> latitud;
        $longitud = $request -> longitud;
        //FerreteriaModel::insert(['id'=> $id, 'razon_social'=>$razon_social, 'giro' => $giro, 'domicilio_fiscal' => $domicilio_fiscal, 
        //'rfc' =>$rfc, 'estado'=>$estado, 'ano_ingreso' =>$ano_ingreso,'bandera'=>$bandera ]);

        manufactura::insert(['id_unieco' => $id_unieco,'nom_estab' => $nom_estab,'raz_social' => $raz_social,
            'id_actividad' => $id_actividad,'nom_vial' =>$nom_vial, 'tipocencom'=>$tipocencom,'nom_e'=>$nom_e,
            'nomb_asent' => $nomb_asent, 'cod_postal' => $cod_postal, 'telefono' => $telefono, 'latitud' =>$latitud, 'longitud'=>$longitud]);
        return response()->json(['mensaje' => 'Registrado correctamente',
        ]);
    }

    public function actualizar_black(Request $request){
        $id_unieco = $request-> id_unieco;
        $nom_estab = $request-> nom_estab;
        $raz_social = $request -> raz_social;
        $id_actividad = $request-> id_actividad;
        $nom_vial = $request-> nom_vial;
        $tipocencom = $request -> tipocencom;
        $nom_e = $request -> nom_e;
        $nomb_asent = $request -> nomb_asent;
        $cod_postal = $request-> cod_postal;
        $telefono = $request-> telefono;
        $latitud = $request -> latitud;
        $longitud = $request -> longitud;

        $editar = manufactura::select('id_unieco','nom_estab','raz_social','id_actividad','nom_vial','tipocencom',
        'nom_e','nomb_asent','cod_postal','telefono','latitud','longitud')

        ->where('id_unieco','=', $id_unieco)

        ->update(['id_unieco' => $id_unieco,'nom_estab' => $nom_estab,'raz_social' => $raz_social,
        'id_actividad' => $id_actividad,'nom_vial' =>$nom_vial, 'tipocencom'=>$tipocencom,'nom_e'=>$nom_e,
        'nomb_asent' => $nomb_asent, 'cod_postal' => $cod_postal, 'telefono' => $telefono, 'latitud' =>$latitud, 'longitud'=>$longitud]);
        return response()->json(['mensaje' => 'actualizado  correctamente',]);

    }

    public function eliminar_black(Request $request){
        $id_unieco = $request-> id_unieco;
        /*$nom_estab = $request-> nom_estab;
        $raz_social = $request -> raz_social;
        $id_actividad = $request-> id_actividad;
        $nom_vial = $request-> nom_vial;
        $tipocencom = $request -> tipocencom;
        $nom_e = $request -> nom_e;
        $nomb_asent = $request -> nomb_asent;
        $cod_postal = $request-> cod_postal;
        $telefono = $request-> telefono;
        $latitud = $request -> latitud;
        $longitud = $request -> longitud;*/

        $editar = manufactura::select('id_unieco','nom_estab','raz_social','id_actividad','nom_vial','tipocencom',
        'nom_e','nomb_asent','cod_postal','telefono','latitud','longitud')

        ->where('id_unieco','=', $id_unieco)

        ->delete(['id_unieco','=', $id_unieco]);
        return response()->json(['mensaje' => 'Eliminado  correctamente',]);

    }
    //buscar RAZON SOCIAL
    public function razon_social(Request $request){
        $raz_social= $request->raz_social;
        return response()-> json($consulta = manufactura::select('id_unieco','nom_estab','raz_social','id_actividad','nom_vial','tipocencom',
        'nom_e','nomb_asent','cod_postal','telefono','latitud','longitud')
        ->where('raz_social','=',$raz_social))
        ;
        //->take(1)->first());
    }

    //insertar 4 columnas
    public function insertar_cuatro(Request $request ){
        $id_unieco = $request-> id_unieco;
        $nom_estab = $request-> nom_estab;
        $raz_social = $request -> raz_social;
        $id_actividad = $request-> id_actividad;
        //FerreteriaModel::insert(['id'=> $id, 'razon_social'=>$razon_social, 'giro' => $giro, 'domicilio_fiscal' => $domicilio_fiscal, 
        //'rfc' =>$rfc, 'estado'=>$estado, 'ano_ingreso' =>$ano_ingreso,'bandera'=>$bandera ]);

        manufactura::insert(['id_unieco' => $id_unieco,'nom_estab' => $nom_estab,'raz_social' => $raz_social,
            'id_actividad' => $id_actividad]);
        return response()->json(['mensaje' => 'Registrado correctamente',
        ]);
    }

    public function obtenerApi($id){
        $respuesta = $this->peticion('GET',"http://myapidsos.herokuapp.com/eliminar_ws/{$id}");
        $datos = json_decode($respuesta);
        return response()->json($datos);

    }

    public function insertar(){
        $respuesta = $this->peticion('POST',"http://dsos.com/api/auth/insertar_cuatro",[
           'headers' => [
              'Content-Type' => 'application/x-www-form-urlencoded',
              'X-Requested-With' => 'XMLHttpRequest'
           ],
           'form_params' =>[
              'id_unieco' => '1458',
              'nom_estab' => 'Fortnite',
              'raz_social' => 'Fortnite',
              'id_actividad' => '1212',
              'nom_vial' => 'no',
                'tipocencom' => 'quiero',
                'nom_e' => 'reprobar',
                'nomb_asent' => ':c',
                'cod_postal' => 'quiero',
                'telefono' => 'poder',
                'latitud' => 'dormir',
                'longitud' => ':C'
           ]
        ]);
  
        $datos = json_decode($respuesta);
        return response()->json($datos);
     }

     public function apiActualiza(){
         $respuesta = $this->peticion('PUT',"http://dsos.com/api/auth/actualizar",[
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'X-Requested-With' => 'XMLHttpRequest'
             ],
             'form_params' =>[
                'id_unieco' => '111',
                'nom_estab' => 'okokoko',
                'raz_social' => 'okokoe',
                'id_actividad' => '1212'
             ]
          ]);
          $datos = json_decode($respuesta);
          return response() -> json($datos);
     }

     public function apiElimina(){
         $respuesta = $this->peticion('PUT',"http://dsos.com/api/auth/eliminar",[
             'headers'=>[
                'Content-Type' => 'application/x-www-form-urlencoded',
                'X-Requested-With' => 'XMLHttpRequest'
             ],
             'form_params' =>[
              'id_unieco' => '6',
             ]
         ]);

         $datos = json_decode($respuesta);
         return response()->json($datos);
     }


     /*****************************APIS examen********************************/
     //examen otro 
     public function api_Elimina($id){
        $respuesta = $this->peticion('PUT',"http://myapidsos.herokuapp.com/api/auth/eliminar?id={$id}",[
            'headers'=>[
               'Content-Type' => 'application/x-www-form-urlencoded',
               'X-Requested-With' => 'XMLHttpRequest'
            ],
            'form_params' =>[
             'id' => $id,
            ]
        ]);

        $datos = json_decode($respuesta);
        return response()->json($datos);
    }


    public function actualizar_api( $id){
        $respuesta = $this->peticion('PUT',"http://myapidsos.herokuapp.com/api/auth/actualizar?id={$id}",[
        'headers'=>[
            'Content-Type' => 'application/x-www-form-urlencoded',
            'X-Requested-With' => 'XMLHttpRequest'
         ],
         'form_params' =>[
          'nombre_act' => 'si jala',
          'raz_social'=> 'ayudaaaa :B',
          'nom_vial'=>'ya salió'
    ]]);
  
        $datos = json_decode($respuesta);
        return response()->json($datos);
     }


     public function insertar_api(){
        $respuesta = $this->peticion('POST',"http://myapidsos.herokuapp.com/api/auth/insertar",[
        'headers'=>[
            'Content-Type' => 'application/x-www-form-urlencoded',
            'X-Requested-With' => 'XMLHttpRequest'
         ],
         'form_params' =>[
          'raz_social' => 'esto ya quedo',
          'nombre_act'=> 'queremos',
          'nom_vial'=>'pasar y dormir :c '
    ]]);
  
        $datos = json_decode($respuesta);
        return response()->json($datos);
     }

     public function ver_api( $id){
        $respuesta = $this->peticion('GET',"http://myapidsos.herokuapp.com/api/auth/buscar?id={$id}");
  
        $datos = json_decode($respuesta);
        return response()->json($datos);
     }
}


?>