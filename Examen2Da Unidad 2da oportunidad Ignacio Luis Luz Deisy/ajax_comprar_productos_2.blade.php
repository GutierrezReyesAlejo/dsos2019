<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>COMPRAR PRODUCTO</title>
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/vender_productos_ex.js"></script>
    
</head>
<body>
    <table>
        <tr>
            <td>{!! Form::Label('Nombre: ')!!}</td>
            <td>
                <input type="text" id="nombre"/>
            </td>
        </tr> 
        
        <tr>
            <td>{!! Form::Label('Precio: ')!!}</td>
            <td>
                <input type="text" id="precio"/>
            </td>
        </tr> 
        <tr>
            <td>{!! Form::Label('Edad: ')!!}</td>
            <td>
                <input type="text" id="edad"/>
            </td>
        </tr> 
        
        <tr>
            <td>{!! Form::Label('Costo Inicial: ')!!}</td>
            <td>
                <input type="text" id="costo_inicial"/>
            </td>
        </tr> 

        <tr>
            <td>{!! Form::Label('Cantidad: ')!!}</td>
            <td>
                <input type="number" id="cantidad"/>
            </td>
        </tr> 

        <tr>
            <td>{!! Form::Label('Costo Total: ')!!}</td>
            <td>
                <input type="text" id="total"/>
            </td>
        </tr>
    </table>

    <button onclick="operacion()">Calcular</button>

    <!--'id','nombre','tipo','proveedor','precio_u','precio_v'-->
</body>
</html>