<?php
namespace App\Http\Controllers\PW_Controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\PW\AlumnoModel;

class AlumnoController extends Controller{
    public function verDato(){
        $practica1 = AlumnoModel::select('nombre','apellidoP','apellidoM','edad','direccion','telefono')->take(1)->first();
        return view('PW/alumno_info')->with('variable',$practica1);
    }
}
?>