<?php

namespace App\http\Models\PW;

use Illuminate\Database\Eloquent\Model;

class PracticaModelo extends Model
{

    //Tabla
    protected $table = 'practicas';
    //campos de la tabla
    protected $prymarykey = 'id';
    public $timestamps = false;

    protected $filltable = [
        'id','nombre'
    ];
}


?>