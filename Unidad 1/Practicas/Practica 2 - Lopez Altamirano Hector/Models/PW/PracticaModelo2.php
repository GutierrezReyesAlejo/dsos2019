<?php

namespace App\http\Models\PW;

use Illuminate\Database\Eloquent\Model;

class PracticaModelo2 extends Model
{

    //Tabla
    protected $table = 'alumno';
    //campos de la tabla
    //protected $prymarykey = 'id';
    public $timestamps = false;

    protected $filltable = [
        'nombreAlumno','apellidoPaterno','apellidoMaterno','edad','direccion','telefono'
    ];
}


?>