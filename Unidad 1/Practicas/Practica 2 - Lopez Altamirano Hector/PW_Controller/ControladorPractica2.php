<?php

namespace App\http\Controllers\PW_Controller;

use Illuminate\http\Request;

use App\http\Controllers\Controller;

use App\http\Models\PW\PracticaModelo2;

class ControladorPractica2 extends Controller
{
    public function practica2()
    {
        $practica2 = PracticaModelo2::
            select('nombreAlumno','apellidoPaterno','apellidoMaterno','edad','direccion','telefono')->first();

            return view('PW/VerDatosPractica2')->with('variable',$practica2);
    }
}


?>