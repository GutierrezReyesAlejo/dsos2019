<?php

namespace App\http\Controllers\PW_Controller;

use Illuminate\http\Request;

use App\http\Controllers\Controller;

use App\http\Models\PW\PracticaModelo;

class MiControlador extends Controller
{
    public function index()
    {
        return view('PW/login');
    }

    public function verDatos()
    {
        $practica1 = PracticaModelo::select('id','nombre')->get();
        return $practica1;    
    }

    public function verDatos2()
    {
        $practica2 = PracticaModelo::
            select('id','nombre')->take(1)->first();

            return view('PW/ver_datos')->with('variable',$practica2);
    }
}


?>