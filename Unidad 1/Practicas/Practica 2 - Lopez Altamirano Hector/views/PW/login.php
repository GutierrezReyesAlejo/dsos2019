<?php

?>
<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>

<style>
    body{
        background-color: #008CBA;

    }

    .login{
        margin: auto;
        width: 30%;
        padding: 10px;
        background-color: white;
        
        padding: 30px;
        text-align: center;
    }

    .ingresoDatos{
        color: #008CBA;
        font-family: verdana;
        font-size: 200%;
    }

    .boton{
        background-color: #008CBA;
        color: white;
        border: none;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
    }


</style>

</head>
    <body>


    <div class="login">

    <div class="imagen">
      
        <img src="img_avatar2.png" alt="Avatar" class="avatar">
    </div>

    <form>

        <div class="ingresoDatos">
            <label for="usuario"><b>Usuario</b></label>
            <br>
            <input type="text"  name="inputUsuario">
            <br>
            <br>
            <label for="contraseña"><b>Contraseña</b></label>
            <br>
            <input type="password"  name="inputContraseña">
            <br>
            <br>
            <button class="boton" type="submit">Login</button>
    
        </div>
    </form>

    </body>
</html>