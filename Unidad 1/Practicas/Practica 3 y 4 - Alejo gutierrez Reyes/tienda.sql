/*
 Navicat Premium Data Transfer

 Source Server         : ITO
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : dsos

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 24/02/2019 19:20:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tienda
-- ----------------------------
DROP TABLE IF EXISTS `tienda`;
CREATE TABLE `tienda`  (
  `id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  `razon_social` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `rfc` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `nombre_dueno` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `direccion_dueno` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `tipo_empresa` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `telefono` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `fecha_ingreso` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tienda
-- ----------------------------
INSERT INTO `tienda` VALUES (1, '3232wqw', '0090909', '0', '09kj', 'jjkj0i', '00909', '2019-02-04');
INSERT INTO `tienda` VALUES (2, 'isaiodaoiasoi', 'iooojoioi', 'iooioioi', 'oimkkn', 'kkm,mm', '9778778', '2013-06-04');
INSERT INTO `tienda` VALUES (3, 'Que onda que pez', 'rer2wd', 'juan', 'su ksa', 'no c', '998939823', '2015-10-06');
INSERT INTO `tienda` VALUES (4, 'Pescado Frito', 'YN5C0CUF0', 'SAUL VELAZQUES', 'SU KASA', 'GANADERIA', '094390439009', '2012-10-09');
INSERT INTO `tienda` VALUES (5, 'Mermeladas', '984is98ds98', 'Santiago Durango', 'Santiago de chile', 'Sociedad Cooperativa', '9763738282', '2019-02-19');

SET FOREIGN_KEY_CHECKS = 1;
