<?php
namespace App\Http\Models\PW;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Modelo_practica3 extends Model{
    //nombre de la tabla
    protected $table = 'tienda';

    //llave primaria
    protected $primarykey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'razon_social',
        'rfc',
        'nombre_dueno',
        'direccion_dueno',
        'tipo_empresa',
        'telefono',
        'fecha_ingreso'
    ];
}
