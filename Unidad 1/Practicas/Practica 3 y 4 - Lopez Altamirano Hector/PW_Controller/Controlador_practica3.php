<?php
namespace App\Http\Controllers\PW_Controller;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\PW\Modelo_practica3;
class Controlador_practica3 extends Controller{

    public function CargarDatos(){       
        $practica3 = Modelo_practica3::
            select('id',
                   'razon_social',
                   'rfc',
                   'nombre_dueno',
                   'direccion_dueno',
                   'tipo_empresa',
                   'telefono',
                   'fecha_ingreso')->get();
            return view('PW/Vista_Tienda')->with('tiendas', $practica3);
    }

    public function insertarDatos(request $request){
        $razonsocial = $request->input('razon_social');
                $rfc = $request->input('rfc');
        $nombredueno = $request->input('nombre_dueno');
          $direccion = $request->input('direccion_dueno');
               $tipo = $request->input('tipo_empresa');
           $telefono = $request->input('telefono');
              $fecha = $request->input('fecha_ingreso');

        Modelo_practica3::create([
            'razon_social'=> $razonsocial,
                     'rfc'=> $rfc, 
            'nombre_dueno'=> $nombredueno,
         'direccion_dueno'=> $direccion,
            'tipo_empresa'=> $tipo,
                'telefono'=> $telefono,
           'fecha_ingreso'=> $fecha
            ]);
        return redirect()->to('ver_Tiendas');
    }

    public function cargarFormularioTienda(){
        return  view('PW/Formulario_tienda');
    } 

     public function editarDatos($id){

         $uno = Modelo_practica3::
         where('id', $id)->take(1)->first();

        return view('PW/Actualizar_Formulario')->with('tienda', $uno);
     }

     public function hacerActualizacion(request $data, $id){
         $editar = Modelo_practica3::find($id);

         $editar->razon_social = $data->razon_social;
         $editar->rfc = $data->rfc;
         $editar->nombre_dueno = $data->nombre_dueno;
         $editar->direccion_dueno = $data->direccion_dueno;
         $editar->tipo_empresa = $data->tipo_empresa;
         $editar->telefono = $data->telefono;
         $editar->fecha_ingreso = $data->fecha_ingreso;

         $editar->save();

         return redirect()->to('ver_Tiendas');
     }
}


?>
