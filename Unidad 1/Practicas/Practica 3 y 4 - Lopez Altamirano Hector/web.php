<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('Hola','PW_Controller\micontrolador@index');

Route::get('/', function () {
    return view('welcome');
});

Route::get('ver_datos','PracticaController\PracticaController@ver_datos');
Route::get('visualizar','PracticaController\PracticaController@ver_datos');
Route::get('verpractica2','Practica2Controller\Controlador@ver_datos');
Route::get('formulario','Practica2Controller\Controlador@ver_formulario');
Route::post('insertar','Practica2Controller\Controlador@insertar');
//Practica 3
Route::post('insertarDatos', 'PW_Controller\Controlador_practica3@insertarDatos');
Route::get('ver_Tiendas','PW_Controller\Controlador_practica3@CargarDatos');
Route::get('nuevatienda', 'PW_Controller\Controlador_practica3@cargarFormularioTienda');
//Practica 4
Route::get('editarTienda/{id}', 'PW_Controller\Controlador_practica3@editarDatos');
Route::put('putTienda/{id}', 'PW_Controller\Controlador_practica3@hacerActualizacion');
