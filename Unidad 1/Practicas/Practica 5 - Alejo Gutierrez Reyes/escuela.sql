/*
 Navicat Premium Data Transfer

 Source Server         : ITO
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : dsos

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 24/02/2019 18:23:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for escuela
-- ----------------------------
DROP TABLE IF EXISTS `escuela`;
CREATE TABLE `escuela`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rfc` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `curp` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `num_ctrl` varchar(8) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `cal1` decimal(3, 1) NULL DEFAULT NULL,
  `cal2` decimal(3, 1) NULL DEFAULT NULL,
  `cal3` decimal(3, 1) NULL DEFAULT NULL,
  `promedio` decimal(3, 1) NULL DEFAULT NULL,
  `fecha` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of escuela
-- ----------------------------
INSERT INTO `escuela` VALUES (1, 'gura960505hoct', 'gura960505hoct', '65889343', 10.0, 6.8, 7.8, 8.3, '2019-02-20');
INSERT INTO `escuela` VALUES (2, 'carlosekjhghjk', 'jksui98dsmjg8', '95839223', 10.0, 6.8, 7.8, 8.3, '2019-02-20');
INSERT INTO `escuela` VALUES (3, '8g9d75a5a53j5j', 'carlangas', '15161306', 10.0, 9.7, 7.8, 8.3, '2019-02-20');
INSERT INTO `escuela` VALUES (4, '80f0g0f8d7s8d6', '6s7f9d0s03m3m', '14939910', 10.0, 6.8, 7.8, 8.3, '2019-02-20');
INSERT INTO `escuela` VALUES (5, 'khejdskjdskjds', 'ljsdljdsljdsjsd', '7272828', 10.0, 10.0, 10.0, NULL, '2019-02-07');
INSERT INTO `escuela` VALUES (6, 'DGOEMD82382', 'JDU289EJKK92M3N', '82736281', 10.0, 10.0, 78.8, NULL, '2019-01-28');

SET FOREIGN_KEY_CHECKS = 1;
