<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('Hola','PW_Controller\micontrolador@index');

Route::get('/', function () {
    return view('welcome');
});

Route::get('ver_datos','PracticaController\PracticaController@ver_datos');

Route::get('visualizar','PracticaController\PracticaController@ver_datos');

Route::get('verpractica2','Practica2Controller\Controlador@ver_datos');


Route::get('formulario','Practica2Controller\Controlador@ver_formulario');

Route::post('insertar','Practica2Controller\Controlador@insertar');

/**
 * ----------------------------------------
 *          Rutas para la practica 3
 * ----------------------------------------
 */

 Route::post('InsertarTienda', 'Practica3Controller\ControladorTienda@insertarTienda');

 Route::get('verTiendas','Practica3Controller\ControladorTienda@VerDatosDeBD');

 Route::get('nuevatienda', 'Practica3Controller\ControladorTienda@load_form_tienda');

     /** ---------------------------------------------
     * |                                            |
     * |                Practica 4                  |
     * |                                            |  
     * | Descripcion:  'Update'                     |
     * |                                            |
     * ----------------------------------------------
     */
Route::get('actualizartienda/{id}', 'Practica3Controller\ControladorTienda@editar_datos');



    /** ---------------------------------------------
     * |                                            |
     * |                Practica 5                  |
     * |                                            |  
     * | Descripcion:  'Escuela'                    |
     * |                                            |
     * ----------------------------------------------
     */
    Route::get('listaEscuelas', 'Practica4Controller\ControladorEscuela@openViewEscuela');

    Route::get('nuevaEscuela', 'Practica4Controller\ControladorEscuela@openInsertForm');
    Route::post('postNewEscuela', 'Practica4Controller\ControladorEscuela@doPostNewEscuela');

    Route::get('editEscuela/{id}', 'Practica4Controller\ControladorEscuela@openEditForm');
    Route::put('putEscuela/{id}', 'Practica4Controller\ControladorEscuela@doUpdateEscuela');