<?php
    namespace app\Http\Controllers\PW_controler;
    
    use Illuminate\Http\Request;

    use App\Http\Controllers\Controller;

    use App\Models\PW\Modelo_practica4;

    class Controlador_practica4 extends Controller
    {
        public function verDatos()
        {
            $practica4 = Modelo_practica4::
            select('id','rfc','curp','materia1','calif_m1','materia2','calif_m2','materia3','calif_m3','promedio','fecha')->take(1)->first();
            return view('PW/VistaVerDatos_practica4')->with('variable',$practica4);
        }

       public function verFormulariop4() 
       {
           return view ("PW\VistaInsertar_practica4");
       }

        public function insertardatosp4(Request $request)
        {
                  $id = $request->input('id');
                 $rfc = $request->input('rfc');
                $curp = $request->input('curp');
            $num_ctrl = $request->input('num_ctrl');
            $materia1 = $request->input('materia1');
            $calif_m1 = $request->input('calif_m1');
            $materia2 = $request->input('materia2');
            $calif_m2 = $request->input('calif_m2');
            $materia3 = $request->input('materia3');
            $calif_m3 = $request->input('calif_m3');
            $promedio = $request->input('promedio');
               $fecha = $request->input('fecha');

            Modelo_practica4::create(['id'=>$id,'rfc'=>$rfc,'curp'=>$curp,'num_ctrl'=>$num_ctrl,'materia1'=>$materia1,'calif_m1'=>$calif_m1,'materia2'=>$materia2,'calif_m2'=>$calif_m2,'materia3'=>$materia3,'calif_m3'=>$calif_m3,'promedio'=>$promedio,'fecha'=>$fecha]);
            return redirect()->to('verdatosp4');//a donde debe de ir despues de realizar la insercion
        }//(redirigir hacia la vista que muestra el primer registro)

        public function editDatosp4($id)
        {
            $uno = Modelo_practica4::where('id',$id)->take(1)->first();//primera opcion para editar un campo

            //$dos = modelop3::find($id);//segunda opcion para modificar un dato

            return view('PW\VistaActualizar_practica4')//modificarp3 es la vista para ver los datos solicitados
                ->with('uno', $uno);
        }

        public function actualizarDatosp4(Request $data, $id)
        {
            $editar = Modelo_practica4::find($id);

                  $editar->id = $data->id;
                 $editar->rfc = $data->rfc;
                $editar->curp = $data->curp;
            $editar->materia1 = $data->materia1;
            $editar->calif_m1 = $data->calif_m1;
            $editar->materia2 = $data->materia2;
            $editar->calif_m2 = $data->calif_m2;
            $editar->materia3 = $data->materia3;
            $editar->calif_m3 = $data->calif_m3;
            $editar->promedio = $data->promedio;
               $editar->fecha = $data->fecha;

            $editar->save();

            return redirect()->to('verdatosp4');
        }

    }
    ?>