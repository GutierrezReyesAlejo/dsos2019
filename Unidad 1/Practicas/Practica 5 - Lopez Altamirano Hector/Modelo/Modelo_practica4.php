<?PHP

namespace App\Models\PW;

use Illuminate\Database\Eloquent\Model;

class Modelo_practica4 extends Model
{

    protected $table = 'practica4';

    protected $primarykey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'id','rfc','curp','num_ctrl','materia1','calif_m1','materia2','calif_m2','materia3','calif_m3','promedio','fecha'
    ];
}