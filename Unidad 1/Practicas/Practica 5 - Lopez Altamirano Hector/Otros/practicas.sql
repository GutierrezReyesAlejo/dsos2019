-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-02-2019 a las 22:02:00
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `practicas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `practica4`
--

CREATE TABLE `practica4` (
  `id` int(11) NOT NULL,
  `rfc` text NOT NULL,
  `curp` text NOT NULL,
  `materia1` text NOT NULL,
  `calif_m1` int(11) NOT NULL,
  `materia2` text NOT NULL,
  `calif_m2` int(11) NOT NULL,
  `materia3` text NOT NULL,
  `calif_m3` int(11) NOT NULL,
  `promedio` float NOT NULL,
  `fecha` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `practica4`
--

INSERT INTO `practica4` (`id`, `rfc`, `curp`, `materia1`, `calif_m1`, `materia2`, `calif_m2`, `materia3`, `calif_m3`, `promedio`, `fecha`) VALUES
(1, 'LOAH970630HOCPLC', 'LOAH970630HOCPLC06', 'DSOS', 100, 'Administracion de redes', 90, 'Ciencia de los datos', 100, 0, '02/02/2019');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `practicas`
--

CREATE TABLE `practicas` (
  `id` int(11) NOT NULL,
  `nombre` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `practicas`
--

INSERT INTO `practicas` (`id`, `nombre`) VALUES
(1, 'Hectoor :D'),
(2, 'Ola ke ase '),
(45, 'hola');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `practica4`
--
ALTER TABLE `practica4`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `practicas`
--
ALTER TABLE `practicas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `practica4`
--
ALTER TABLE `practica4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
