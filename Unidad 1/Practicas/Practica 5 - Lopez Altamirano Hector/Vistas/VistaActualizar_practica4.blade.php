<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Actualizar Datos</title>
</head>
<body>

    {!!Form::open(array('url'=>'actualizarDatosp4/'.$uno->id,'method'=>'PUT'
    ,'autocomplete'=>'off'))!!}

    {!!Form::label('ID: ') !!}
    {!!Form::text('id',$uno->id)!!}
    <br>
    {!!Form::label('RFC: ')!!}
    {!!Form::text('rfc',$uno->rfc)!!}
    <br>
    {!!Form::label('Curp: ')!!}
    {!!Form::text('curp',$uno->curp)!!}
    <br>
    {!!Form::label('Numero de Control: ')!!}
    {!!Form::text('num_ctrl',$uno->num_ctrl)!!}
    <br>
    {!!Form::label('Materia1: ')!!}
    {!!Form::select('materia1', array('Español' => 'Español', 'Matematicas' => 'Metematicas', 'Quimica' => 'Quimica'), $uno->materia1)!!}
    <br>
    {!!Form::label('Calificacion 1: ')!!}
    {!!Form::text('calif_m1',$uno->calif_m1)!!}
    <br>
    {!!Form::label('Materia2: ')!!}
    {!!Form::select('materia2', array('Español' => 'Español', 'Matematicas' => 'Metematicas', 'Quimica' => 'Quimica'), $uno->materia2)!!}
    <br>
    {!!Form::label('Calificacion 2: ')!!}
    {!!Form::text('calif_m2',$uno->calif_m2)!!}
    <br>
    {!!Form::label('Materia3: ')!!}
    {!!Form::select('materia3', array('Español' => 'Español', 'Matematicas' => 'Metematicas', 'Quimica' => 'Quimica'), $uno->materia3)!!}
    <br>
    {!!Form::label('Calificacion 3: ')!!}
    {!!Form::text('calif_m3',$uno->calif_m3)!!}
    <br>
    {!!Form::label('Promedio: ')!!}
    {!!Form::text('promedio',$uno->promedio)!!}
    <br>
    {!!Form::label('Fecha: ')!!}
    {!!Form::text('fecha',$uno->fecha)!!}
    <br>

    {!!form::submit('Actualizar',['id'=>'act','rfc'=>'act','curp'=>'act','num_ctrl'=>'act','materia1'=>'act','calif_m1'=>'act','materia2'=>'act','calif_m2'=>'act','materia3'=>'act','calif_m3'=>'act','promedio'=>'act','fecha'=>'act','content'=>'<span>Actualizar</span>'])!!}

        {!!Form::close()!!}

</body>
</html>