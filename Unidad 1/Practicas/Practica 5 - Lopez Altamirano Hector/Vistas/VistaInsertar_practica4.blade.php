<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>InsertarDatos</title>
</head>
<body>
    
    {!!Form::open(array('url'=>'insertardatosp4','method'=>'POST','autocomplete'=>'off'))!!}

    {!!Form::label('RFC: ')!!}
    {!!Form::text('rfc',null)!!}
    <br>
    {!!Form::label('Curp: ')!!}
    {!!Form::text('curp',null)!!}
    <br>
    {!!Form::label('Numero de Control: ')!!}
    {!!Form::text('num_ctrl',null)!!}
    <br>
    {!!Form::label('Materia1: ')!!}
    {!!Form::select('materia1', array('Español' => 'Español', 'Matematicas' => 'Metematicas', 'Quimica' => 'Quimica'), null)!!}
    <br>
    {!!Form::label('Calificacion 1: ')!!}
    {!!Form::text('calif_m1',null)!!}
    <br>
    {!!Form::label('Materia2: ')!!}
    {!!Form::select('materia2', array('Español' => 'Español', 'Matematicas' => 'Metematicas', 'Quimica' => 'Quimica'), null)!!}
    <br>
    {!!Form::label('Calificacion 2: ')!!}
    {!!Form::text('calif_m2',null)!!}
    <br>
    {!!Form::label('Materia3: ')!!}
    {!!Form::select('materia3', array('Español' => 'Español', 'Matematicas' => 'Metematicas', 'Quimica' => 'Quimica'), null)!!}
    <br>
    {!!Form::label('Calificacion 3: ')!!}
    {!!Form::text('calif_m3',null)!!}
    <br>
    {!!Form::label('Promedio: ')!!}
    {!!Form::text('promedio',null)!!}
    <br>
    {!!Form::label('Fecha: ')!!}
    {!!Form::date('fecha',null)!!}
    <br>

    {!!Form::submit('Registrar',['id'=>'grabar','rfc'=>'grabar','curp'=>'grabar','num_ctrl'=>'grabar','materia1'=>'grabar','calif_m1'=>'grabar','materia2'=>'grabar','calif_m2'=>'grabar','materia3'=>'grabar','calif_m3'=>'grabar','promedio'=>'grabar','fecha'=>'grabar','content'=>'<span>Insertar</span>'])!!}

    {!!Form::close()!!}

</body>
</html>