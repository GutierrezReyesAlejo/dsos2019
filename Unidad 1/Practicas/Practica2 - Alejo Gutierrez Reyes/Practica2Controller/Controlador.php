<?php
namespace App\Http\Controllers\Practica2Controller;

use App\Http\Controllers\Controller;

use App\Http\Models\Practica2Model\Modelo;

class Controlador extends Controller{
    public function ver_datos(){
       
        $practica2 = Modelo::
            select('nombre', 'apellido_paterno', 'apellido_materno')->take(1)->first();

            return view('Practica2View/Vista')->with('variable', $practica2);
    }
}


?>