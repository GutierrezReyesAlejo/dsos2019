<?php
namespace App\Http\Models\Practica2Model;

use Illuminate\Database\Eloquent\Model;

class Modelo extends Model{
    //nombre de la tabla
    protected $table = 'practica2';

    //llave primaria
    protected $primarykey = 'nombre';
    public $timestamps = 'false';

    //aqui los elementos a mostrarse en la tabla 
    protected $filltable = ['nombre','apellido_paterno', 'apellido_materno'];
}