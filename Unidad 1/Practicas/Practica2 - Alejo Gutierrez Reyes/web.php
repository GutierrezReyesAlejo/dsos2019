<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('Hola','PW_Controller\micontrolador@index');

Route::get('/', function () {
    return view('welcome');
});

Route::get('ver_datos','PracticaController\PracticaController@ver_datos');

Route::get('visualizar','PracticaController\PracticaController@ver_datos');

Route::get('verpractica2','Practica2Controller\Controlador@ver_datos');