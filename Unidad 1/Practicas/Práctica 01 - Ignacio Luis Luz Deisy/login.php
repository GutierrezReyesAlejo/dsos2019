<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Práctica 01 - Login</title>
    <!--JQUERY-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <!-- FRAMEWORK BOOTSTRAP para el estilo de la pagina-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link href="https://fonts.googleapis.com/css?family=Dosis:300,500|Open+Sans+Condensed:300" rel="stylesheet">
</head>
<body> 
    <div class="container-login">
        <div class="wrap-login">
            <h1 class="login-title"> Bienvenido</h1>
            <img class="icon" src="https://image.flaticon.com/icons/svg/149/149068.svg">
            <form> 
                <div class=block>
                    <input type="text" name="email" placeholder="Correo">
                    <span class="focus-input100" data-placeholder="Email"></span>
                </div>
                <div class= block>
                    <input type="password" name="pass" placeholder="Contraseña">    
                </div>
                <div class="custom-control custom-checkbox checkbox" >
                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">Recordarme</label>
                </div>
                <div style="text-align:center;">
                    <button type="button" class="btn btn-primary">Ingresar</button> 
                </div>                   
             </form>
    
        </div>
    </div>
    
</body>
</html>