<?php
namespace App\Http\Controllers\PW_Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\PW\estudiante_modelo;

class estudiante_controlador extends Controller{
    public function ver_datos(){
        $practica1 = estudiante_modelo::
            select('id','nombre','apellido_paterno','apellido_materno','edad','direccion','telefono')->take(1)->first();
            return view ('PW/estudiante_ver_datos')->with('variable',$practica1);
    }
    public function insertar(Request $request){
        $id = $request->input('id');
        $nombre = $request->input('nombre');
        $apellidoP = $request->input('apellido_paterno');
        $apellidoM = $request->input('apellido_materno');
        $edad = $request->input('edad');
        $direccion = $request->input('direccion');
        $telefono = $request->input('telefono');

        estudiante_modelo::insert(['id'=> $id, 'nombre'=>$nombre, 'apellido_paterno' => $apellidoP, 'apellido_materno' => $apellidoM, 'edad' =>$edad, 'direccion'=>$direccion, 'telefono' =>$telefono ]);
        return redirect()->to('/visualizar');//mandarme a vista, poner url
    }
    public function ver_formulario(){
        return view ('PW/estudiante_insertar_datos');
    }

}
?>
