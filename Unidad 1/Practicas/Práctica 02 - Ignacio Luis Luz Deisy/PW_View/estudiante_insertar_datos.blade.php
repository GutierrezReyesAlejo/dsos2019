<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
    {!!Form::open(array('url' => 'insertar', 'method' => 'POST','autocomplete' => 'off'))!!}
    
    {!!Form::label('ID: ')!!}
    <br>
    {!!Form::text('id',null)!!}
    <br>
    {!!Form::label('NOMBRE: ')!!}
    <br>
    {!!Form::text('nombre',null)!!}
    <br>
    {!!Form::label('APELLIDO PATERNO: ')!!}
    <br>
    {!!Form::text('apellido_paterno',null)!!}
    <br>
    {!!Form::label('APELLIDO MATERNO: ')!!}
    <br>
    {!!Form::text('apellido_materno',null)!!}
    <br>
    {!!Form::label('EDAD: ')!!}
    <br>
    {!!Form::text('edad',null)!!}
    <br>
    {!!Form::label('DIRECCIÓn: ')!!}
    <br>
    {!!Form::text('direccion',null)!!}
    <br>
    {!!Form::label('TELÉFONO: ')!!}
    <br>
    {!!Form::text('telefono',null)!!}

    {!!Form::submit('Registrar',['content'=>'<span>Registar</span>'])!!}

    {!!Form::close()!!}
</body>
</html>