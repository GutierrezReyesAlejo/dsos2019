-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generaci�n: 13-02-2019 a las 23:15:17
-- Versi�n del servidor: 10.1.37-MariaDB
-- Versi�n de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `practica2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `estudiante` (
  `id` int(11) NOT NULL,
  `nombreAlumno` text NOT NULL,
  `apellidoPaterno` text NOT NULL,
  `apellidoMaterno` text NOT NULL,
  `Edad` int(11) NOT NULL,
  `Direccion` text NOT NULL,
  `Telefono` bigint(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estudiante`
--

INSERT INTO `estudiante`(`id`, `nombre`, `apellido_paterno`, `apellido_materno`, `edad`, `direccion`, `telefono`) VALUES
(1,'Hector', 'Lopez', 'Altamirano', 21, 'San Martin Mexicapan', 9512512135),
(2,'Luz Deisy', 'Ignacio', 'Luis', 22, 'Santa Rosa', 9518734762),
(3,'Carlos', 'Lopez', 'Rodriguez', 21, 'EL Rosario', 9518923947),
(4,'Erick', 'Andrade', 'Revilla', 21, 'Santa Lucia', 9518943876),
(5,'Saul Renato', 'Aragon', 'Moreyra', 21, 'Atzompa', 9518999264),
(6,'Luis Antonio', 'Lopez', 'Santiago', 21, 'Etla', 9518723784),
(7,'Hugo', 'Estudillo', 'Carre�o', 21, 'Cinco Se�ores', 9517833989),
(8,'Ricardo', 'Rosas', 'Maganda', 21, 'Pueblo Nuevo', 9518734980),
(9,'Alejo', 'Gutierrez', 'Reyes', 22, 'Santa Lucia del Camino', 9519090463),
(10,'Sylvia', 'Velazquez', 'Sanchez', 21, 'El Rosario', 9512398654);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
