<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login','PW_Controller\Mi_Controlador@index');

//práctica 2
Route::get('visualizar','PW_Controller\estudiante_controlador@ver_datos');
Route::get('formulario','PW_Controller\estudiante_controlador@ver_formulario'); //POST porque es fotmulario
Route::post('insertar','PW_Controller\estudiante_controlador@insertar');

Route::get('/', function () {
    return view('welcome');
});
?>