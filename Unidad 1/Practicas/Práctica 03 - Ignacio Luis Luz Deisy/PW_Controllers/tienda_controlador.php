<?php
namespace App\Http\Controllers\PW_Controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\PW\tienda_modelo;

class tienda_controlador extends Controller{
    public function ver_datos(){
        $practica1 = tienda_modelo::
            select('id','razon_social','rfc','nombre_dueno','direccion_dueno','tipo_empresa','telefono','fecha_ingreso')->take(1)->first();
            return view ('PW/tienda_ver_datos')->with('variable',$practica1);
    }

    public function insertar_t(Request $request){
        $id = $request->input('id');
        $razon_social = $request->input('razon_social');
        $rfc = $request->input('rfc');
        $nombre_dueno = $request->input('nombre_dueno');
        $direccion_dueno = $request->input('direccion_dueno');
        $tipo_empresa = $request->input('tipo_empresa');
        $telefono = $request->input('telefono');
        $fecha_ingreso = $request->input('fecha_ingreso');
        
        tienda_modelo::insert(['id'=> $id, 'razon_social' => $razon_social, 'rfc' => $rfc, 'nombre_dueno' =>$nombre_dueno, 'direccion_dueno'=>$direccion_dueno,'tipo_empresa'=>$tipo_empresa, 'telefono' =>$telefono,'fecha_ingreso'=>$fecha_ingreso ]);
        return redirect()->to('/ver_tienda');
    }

    public function ver_formulario(){
        return view('PW\tienda_insertar');
    }

}
?>
