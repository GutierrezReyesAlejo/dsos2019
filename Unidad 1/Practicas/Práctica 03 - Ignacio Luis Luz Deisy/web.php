<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login','PW_Controller\Mi_Controlador@index');

//práctica 2
Route::get('visualizar','PW_Controller\estudiante_controlador@ver_datos');
Route::get('formulario','PW_Controller\estudiante_controlador@ver_formulario'); 
Route::post('insertar','PW_Controller\estudiante_controlador@insertar');//POST porque es fotmulario

//práctica 3
Route::get('ver_tienda','PW_Controller\tienda_controlador@ver_datos');
Route::get('formulario_t','PW_Controller\tienda_controlador@ver_formulario');
Route::post('insertar_t','PW_Controller\tienda_controlador@insertar_t'); //POST porque es fotmulario

Route::get('/', function () {
    return view('welcome');
});
?>