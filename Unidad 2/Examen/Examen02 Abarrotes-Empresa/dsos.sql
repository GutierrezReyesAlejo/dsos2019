-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-04-2019 a las 01:22:32
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dsos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE `alumnos` (
  `id` int(11) NOT NULL,
  `nombre_completo` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sexo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno_materia`
--

CREATE TABLE `alumno_materia` (
  `id` int(11) NOT NULL,
  `nombre_alumno` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `numero_control` int(11) DEFAULT NULL,
  `semestre` int(11) DEFAULT NULL,
  `materia` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alumno_materia`
--

INSERT INTO `alumno_materia` (`id`, `nombre_alumno`, `numero_control`, `semestre`, `materia`) VALUES
(1, 'ALEJO GUTIERREZ', 15161306, 5, '16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id` int(11) NOT NULL,
  `rfc` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `razon_social` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccion_fiscal` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `apoderado_fiscal` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id`, `rfc`, `razon_social`, `direccion_fiscal`, `apoderado_fiscal`, `telefono`, `borrado`) VALUES
(1, 'ljdskjsdj', 'DFGVHBJNHVG', 'qkdsjlkdf0idsi', 'kcklkfdlkd', 'kdflkldfk', 0),
(2, 'KJDSJKDSJDS', 'pipidiodsp', 'LSDLKSDLK', 'p\'9ddpo', '032903290', 0),
(3, 'AAAAAAAAA', 'AAAAAAAAAAA', 'AAAAAAAAAA', 'AAAAAAAAA', '8888888', 1),
(4, 'DFGHJ', 'WERTYUI', 'WERTYUI', 'WERTYUI', '4567', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escuela`
--

CREATE TABLE `escuela` (
  `id` int(11) NOT NULL,
  `rfc` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `curp` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `num_ctrl` varchar(8) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cal1` decimal(3,1) DEFAULT NULL,
  `cal2` decimal(3,1) DEFAULT NULL,
  `cal3` decimal(3,1) DEFAULT NULL,
  `promedio` decimal(3,1) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `avalaible` tinyint(1) DEFAULT NULL,
  `deleted_at` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `escuela`
--

INSERT INTO `escuela` (`id`, `rfc`, `curp`, `num_ctrl`, `cal1`, `cal2`, `cal3`, `promedio`, `fecha`, `avalaible`, `deleted_at`) VALUES
(2, 'carlosekjhghjk', 'OILOOOOOOOOOOOOOO', '95839223', '10.0', '6.8', '7.8', '8.3', '2019-02-20', 0, 1),
(3, '8g9d75a5a53j5j', 'carlangas', '15161306', '9.9', '9.7', '7.8', '8.3', '2019-02-20', 0, 1),
(5, 'Oloberhas sisirve', 'ljsdljdsljdsjsd', '7272828', '10.0', '10.0', '10.0', NULL, '2019-02-07', 1, 1),
(6, 'GURA960505HOCTYL07', 'GURA960505HOCTYL07', '7183838', '7.0', '8.0', '5.0', NULL, '2019-03-05', NULL, NULL),
(7, 'HWJEWJ090923J', '90S09SD09KJ32', '98298289', '28.0', '77.0', '77.0', NULL, '1992-07-29', 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ferreteria`
--

CREATE TABLE `ferreteria` (
  `id` int(11) NOT NULL,
  `razon_social` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `giro` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `domicilio_fiscal` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `rfc` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `anio_ingreso` date DEFAULT NULL,
  `borrado_el` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ferreteria`
--

INSERT INTO `ferreteria` (`id`, `razon_social`, `giro`, `domicilio_fiscal`, `rfc`, `estado`, `anio_ingreso`, `borrado_el`) VALUES
(2, 'Comex', 'Pintura', 'No se c bro', '898JJK32JK32J', 'Michoacan', '2025-03-19', 1),
(3, 'Cinemex', 'Movies', 'Plaza bella', '090JK3KJ32KJ', 'Guadalajara', '2016-07-20', 1),
(5, 'POLLOS', 'POLLOS', 'NO C', '73289KJ3JK', 'MEXICO', '2025-09-26', 1),
(6, 'Super Pitico', 'Super Mercado', 'Santa Rosa Panzacola', '789DSGGFY787', 'Oaxaca', '1988-11-12', 1),
(7, 'Bodega Aurrera', 'Super Mercado', 'Carretera Internacional Km 54', 'NHU7897IHKJKJ7', 'Mexico', '1980-12-09', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

CREATE TABLE `materia` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `semestre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `materia`
--

INSERT INTO `materia` (`id`, `nombre`, `semestre`) VALUES
(1, 'POO', 2),
(2, 'DSOS', 8),
(3, 'PLF', 7),
(4, 'DISOR', 9),
(5, 'REDES DE COMPUTADORAS', 5),
(6, 'FUND DE INVESTIGACION', 1),
(7, 'ESTRUCTURAS DE DATOS', 3),
(8, 'TOPICOS AVANZADOS DE POO', 4),
(9, 'PROGRAMACION WEB', 6),
(10, 'GRAFICACION', 6),
(11, 'FUND DE PROGRAMACION', 1),
(12, 'MATEMATICAS DISCRETAS', 2),
(13, 'CALCULO VECTORIAL', 3),
(14, 'TALLER DE BD', 4),
(15, 'SIMULACION', 5),
(16, 'ARQUITECTURA DE COMPUTADORAS', 5),
(17, 'TICS', 6),
(18, 'TALLER 1 DE INVESTIGACION', 7),
(19, 'ADMIN DE REDES', 8),
(20, 'TALLER II DE INVESTIGACION', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `practica2`
--

CREATE TABLE `practica2` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8_spanish_ci NOT NULL,
  `apellido_paterno` text COLLATE utf8_spanish_ci NOT NULL,
  `apellido_materno` text COLLATE utf8_spanish_ci NOT NULL,
  `edad` int(2) DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `practica2`
--

INSERT INTO `practica2` (`id`, `nombre`, `apellido_paterno`, `apellido_materno`, `edad`, `direccion`, `telefono`) VALUES
(0, 'Omar', 'Martinez', 'Aquino', 18, 'Calle Puerto Escondido', '9511075508'),
(1, 'Alejo', 'Gutierrez', 'Reyes', 22, 'Andador', '9511234567'),
(2, 'Victor Hugo', 'Barragan', 'Saucedo', 22, 'Privada de Venustiano Carranza', '9511104710'),
(3, 'Lazaro', 'Cristobal', 'Sanchez', 23, 'Callejo Ignacio Allende', '9511453407'),
(5, 'Gilberto', 'Enriquez', 'Domingues', 23, 'Juan Escutia #302', '9872345893'),
(7, 'Gonzalo', 'Gonzales', 'Guzman', 24, 'Privada Niños Heroes S/N, Fracc. El Rosario', '9511456792');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `practicas`
--

CREATE TABLE `practicas` (
  `id` int(12) NOT NULL,
  `nombre` varchar(23) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `practicas`
--

INSERT INTO `practicas` (`id`, `nombre`) VALUES
(1, 'Alejo'),
(2, 'Carlangas'),
(3, 'Juan Perez');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipo` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `proveedor` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `precio_unitaria` decimal(5,2) DEFAULT NULL,
  `precio_venta` decimal(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `nombre`, `tipo`, `proveedor`, `precio_unitaria`, `precio_venta`) VALUES
(2, 'dsljksdj', 'lkdslkklsd', 'qkdslkdfkl', '12.20', '45.30'),
(3, 'COCA', 'REFRESCOS', 'COCA', '11.00', '15.50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `semestre`
--

CREATE TABLE `semestre` (
  `id` int(11) NOT NULL,
  `semestre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `semestre`
--

INSERT INTO `semestre` (`id`, `semestre`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tienda`
--

CREATE TABLE `tienda` (
  `id` int(4) UNSIGNED NOT NULL,
  `razon_social` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `rfc` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_dueno` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `direccion_dueno` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_empresa` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tienda`
--

INSERT INTO `tienda` (`id`, `razon_social`, `rfc`, `nombre_dueno`, `direccion_dueno`, `tipo_empresa`, `telefono`, `fecha_ingreso`) VALUES
(1, '3232wqw', '0090909', '0', '09kj', 'jjkj0i', '00909', '2019-02-04'),
(2, 'isaiodaoiasoi', 'iooojoioi', 'iooioioi', 'oimkkn', 'kkm,mm', '9778778', '2013-06-04'),
(3, 'Que onda que pez', 'rer2wd', 'juan', 'su ksa', 'no c', '998939823', '2015-10-06'),
(4, 'Pescado Frito', 'YN5C0CUF0', 'SAUL VELAZQUES', 'SU KASA', 'GANADERIA', '094390439009', '2012-10-09'),
(5, 'Mermeladas', '984is98ds98', 'Santiago Durango', 'Santiago de chile', 'Sociedad Cooperativa', '9763738282', '2019-02-19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `alumno_materia`
--
ALTER TABLE `alumno_materia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `escuela`
--
ALTER TABLE `escuela`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ferreteria`
--
ALTER TABLE `ferreteria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `practica2`
--
ALTER TABLE `practica2`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `semestre`
--
ALTER TABLE `semestre`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tienda`
--
ALTER TABLE `tienda`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `alumno_materia`
--
ALTER TABLE `alumno_materia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `escuela`
--
ALTER TABLE `escuela`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `ferreteria`
--
ALTER TABLE `ferreteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `materia`
--
ALTER TABLE `materia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `practica2`
--
ALTER TABLE `practica2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `semestre`
--
ALTER TABLE `semestre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `tienda`
--
ALTER TABLE `tienda`
  MODIFY `id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
