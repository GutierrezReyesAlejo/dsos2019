<?php
namespace App\Http\Controllers\PW_Controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\PW\FerreteriaModel;

class FerreteriaController extends Controller{
    public function verDato(){
        $variable = FerreteriaModel::select('id','razon_social','giro','domicilio_fiscal','rfc','estado','ano_ingreso','bandera')->take(1)->first();
        return view('PW/ferreteria_vista')->with('variable',$variable);
    }

    public function insertView(){
        return view('PW/ferreteria_insertar');
    }

    public function insertar_ferre(Request $request){
        $id = $request->input('id');
        $razon_social = $request->input('razon_social');
        $giro = $request->input('giro');
        $domicilio_fiscal = $request->input('domicilio_fiscal');
        $rfc = $request->input('rfc');
        $estado = $request->input('estado');
        $ano_ingreso = $request->input('ano_ingreso');
        $bandera = $request->input('bandera');

        FerreteriaModel::insert(['id'=> $id, 'razon_social'=>$razon_social, 'giro' => $giro, 'domicilio_fiscal' => $domicilio_fiscal, 'rfc' =>$rfc, 'estado'=>$estado, 'ano_ingreso' =>$ano_ingreso,'bandera'=>$bandera ]);
        return redirect()->to('/visualizar_ferre');//mandarme a vista, poner url
    }


    public function edit_datos_ferre($id){
        $variable=FerreteriaModel::
        where ('id',$id)->take(1)->first();
        $dos = FerreteriaModel::find($id);
        return view('PW/ferreteria_update') ->with('variable',$variable);//nombre de la vista a la que se dirige
                                    //      ->with('dos',$dos); 

    }

    public function actualizar_datos_ferre(Request $request){
        $id = $request->input('id');
        $editar = FerreteriaModel::find($id);
        if(is_null($editar)){
            print('error'.$id);
        }else{
        $editar = FerreteriaModel::find($id);

            $editar->id = $request->id;
            $editar->razon_social = $request->razon_social;
            $editar->giro = $request->giro;
            $editar->domicilio_fiscal = $request->domicilio_fiscal;
            $editar->rfc = $request->rfc;
            $editar->estado = $request->estado;
            $editar->ano_ingreso = $request->ano_ingreso;
            $editar->bandera = $request->bandera;

            $editar->save();

        //estudiante_modelo::insert(['id'=> $id, 'nombre'=>$nombre, 'apellido_paterno' => $apellidoP, 'apellido_materno' => $apellidoM, 'edad' =>$edad, 'direccion'=>$direccion, 'telefono' =>$telefono ]);
        return redirect()->to('/visualizar_ferre');//mandarme a vista, poner url
    }
}


        public function eliminarView_ferre($id){
            $variable= FerreteriaModel::find($id);
            return view('PW/ferreteriaEliminar')->with('variable',$variable);
        }

        public function eliminarDatos_ferre($id){
            $edit = FerreteriaModel::find($id);
            $edit->delete();
            return redirect()->to('/visualizar_ferre');
        }

    public function eliminarViewBandFe($id){
        $variable = FerreteriaModel::find($id);
        return view('PW/ferreteria_eliminar_band')->with('variable',$variable);
    }

    public function eliminarDatosBandFe($id){
        $edit = FerreteriaModel::find($id);
        $edit->bandera=false;
        $edit->save();
        return redirect()->to('/visualizar_ferre');
    }
    public function ver_tablaFe(){
        $vertodo = FerreteriaModel::
        where('bandera','1')->get();
        return view('PW/tablaFerre')->with('usuario',$vertodo);
    }
}
?>