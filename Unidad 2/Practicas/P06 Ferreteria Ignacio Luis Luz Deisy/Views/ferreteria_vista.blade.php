<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Práctica X</title>
</head>
<body>
   
    {!! Form::Label('ID: ')!!}
    <br>
    {!! Form::text('id',$variable->id)!!}
    <br>
    {!! Form::Label('Razón social: ')!!}
    <br>
    {!! Form::text('razon_social',$variable->razon_social)!!}
    <br>
    {!! Form::Label('Giro: ')!!}
    <br>
    {!! Form::text('giro',$variable->giro)!!}
    <br>
    {!! Form::Label('Domicilio fiscal: ')!!}
    <br>
    {!! Form::text('domicilio_fiscal',$variable->domicilio_fiscal)!!}
    <br>
    {!! Form::Label('Rfc: ')!!}
    <br>
    {!! Form::text('rfc',$variable->rfc)!!}
    <br>
    {!! Form::Label('Estado: ')!!}
    <br>
    {!! Form::text('estado',$variable->estado)!!}
    <br>
    {!! Form::Label('Año ingreso: ')!!}
    <br>
    {!! Form::text('ano_ingreso',$variable->ano_ingreso)!!}
    <br>
    {!! Form::Label('Bandera: ')!!}
    <br>
    {!! Form::text('bandera',$variable->bandera)!!}
</body>
</html>
