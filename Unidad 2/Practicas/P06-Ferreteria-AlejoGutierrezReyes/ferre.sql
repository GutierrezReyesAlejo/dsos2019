create table ferreteria(
	id				int primary key auto_increment,
	razon_social 	varchar(50),
	giro			varchar(50),
	domicilio_fiscal varchar(50),
	rfc				varchar(50),
	estado			varchar(50),
	anio_ingreso	date,
	borrado_el		boolean
);