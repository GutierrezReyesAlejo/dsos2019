<?php
namespace App\Http\Controllers\AJAX;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\AJAX\AlumnoModel;
use App\Http\Models\AJAX\Sexo;

use App\Http\Models\AJAX\MateriasModel;
use App\Http\Models\AJAX\SemestreModel;
use App\Http\Models\AJAX\AlumnoMateria;

//
use App\Http\Models\AJAX\TiendaModel;
use App\Http\Models\AJAX\ProductoModel;


class AjaxControllers extends Controller{
    //alumno sexo
    public function listado_alumnos($genero){
        $al = AlumnoModel::select('id','nombre','sexo')
        ->where('sexo',$genero)
        ->get();
        return $al;
    }
    
    public function formu(){
        $enviar = Sexo::pluck('sexo','id');
        return view('AJAX/ejemplo')->with('sex',$enviar);
    }

    // listado materias
    public function listado_materias($semestre){
        $al = MateriasModel::select('id','semestre','materia')
        ->where('semestre',$semestre)
        ->get();
        return $al;
    }
    public function formuMateria(){
        $enviar = SemestreModel::pluck('semestre','id');
        return view('AJAX/materias')->with('sem',$enviar);
    }
    public function inserta(Request $request){
        $nombre = $request->input('nombre');
        $idsemestre = $request['idsemestre'];
        $idmateria = $request['idmateria'];
        \DebugBar::info("hola");
        //protected $filltable = ['id','nombre','semestre','materia'];
        AlumnoMateria::insert(['nombre' => $nombre, 'semestre' => $idsemestre, 'materia' => $idmateria]);
        return redirect()->to('../');
    }

    ///practica abarrotes
    public function ver_formulario(){
        return view('Ajax\inserta_tienda');
    }

    public function insertar_t(Request $request){
        $id = $request->input('id');
        $rfc = $request->input('rfc');
        $razon_social = $request->input('razon_social');
        $direccion_fiscal = $request->input('direccion_fiscal');
        $apoderado_legal = $request->input('apoderado_legal');
        $telefono = $request->input('telefono');
        $estado = $request->input('estado');
        
        TiendaModel::insert(['id'=> $id, 'rfc' => $rfc, 'razon_social' => $razon_social, 'direccion_fiscal'=>$direccion_fiscal,'apoderado_legal'=>$apoderado_legal, 'telefono' =>$telefono,'estado'=>$estado]);
        return redirect()->to('../');
    }

    public function formulario_productos(){
        return view('Ajax\insertar_productos');
    }

    public function insertar_p(Request $request){
        $id = $request->input('id');
        $nombre = $request->input('nombre');
        $tipo = $request->input('tipo');
        $proveedor = $request->input('proveedor');
        $precio_unitario = $request->input('precio_unitario');
        $precio_venta = $request->input('precio_venta');
        
        ProductoModel::insert(['id'=> $id, 'nombre' => $nombre, 'tipo' =>$tipo, 'proveedor' => $proveedor,'precio_unitario'=>$precio_unitario,'precio_venta' =>$precio_venta]);
        return redirect()->to('../');
    }

    public function listado_productos($nombre){
        $al = ProductoModel::select('id','nombre','tipo','proveedor','precio_unitario','precio_venta')
        ->where('id',$nombre)
        ->get();
        return $al;
    }
    public function listado_productos_full(){
        $al = ProductoModel::select('id','nombre','tipo','proveedor','precio_unitario','precio_venta')
        ->get();
        //echo "<script>console.log( 'Debug Objects: " . $al . "' );</script>";

        return $al;
    }
    public function formuProductos(){
        $enviar = ProductoModel::pluck('nombre','id');//el primero es el que 
        //echo "<script>console.log( 'Debug Objects: " . $enviar . "' );</script>";
        //error_log('Some message here.');
        //\DebugBar::info("adas");
        return view('AJAX/ver_productos')->with('sex',$enviar);
    }
    public function insertar_producto(Request $request){
        /*$nombre = $request->input('nombre');
        $idsemestre = $request['idsemestre'];
        $idmateria = $request['idmateria'];
        //protected $filltable = ['id','nombre','semestre','materia'];
        AlumnoMateria::insert(['nombre' => $nombre, 'semestre' => $idsemestre, 'materia' => $idmateria]);
        */
        if ($request->isMethod('post')){
            $nombre = $request->input("nombre");
            var_dump($nombre);
        }
        \DebugBar::info('Holaaaa');
        return redirect()->to('../ajaxProductos');
    }


    public function edit_datos_tienda($id){
        $uno=TiendaModel::
        where ('id',$id)->take(1)->first();
        $dos = TiendaModel::find($id);
        return view('AJAX/editar_tienda') ->with('uno',$uno);//nombre de la vista a la que se dirige
                                    //      ->with('dos',$dos); 

    }

    public function actualizar_datos_tienda(Request $request){
        $id = $request->input('id');
        $dato = TiendaModel::find($id);
        \DebugBar::info($dato);
        if(is_null($dato)){
            print('error'.$id);
        }else{
        $dato = TiendaModel::find($id);

        $dato->rfc = $request->input('rfc');
        $dato->razon_social = $request->input('razon_social');
        $dato->direccion_fiscal = $request->input('direccion_fiscal');
        $dato->apoderado_legal = $request->input('apoderado_legal');
        $dato->telefono = $request->input('telefono');      
        $dato->estado = $request->input('estado');

        $dato->save();
        return redirect()->to('update_tienda/'.$id);
        }
    }

}