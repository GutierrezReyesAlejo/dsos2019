<?php
namespace App\Http\Models\AJAX;
use Illuminate\Database\Eloquent\Model;

class AlumnoMateria extends Model{
    //nombre de la tabla
    protected $table = 'alumno_materia';

    //llave primaria
    protected $primarykey = 'id';
    public $timestamps = false;

    //aqui los elementos a mostrarse en la tabla 
    protected $filltable = ['id','nombre','semestre','materia'];
}