<?php
namespace App\Http\Models\AJAX;
use Illuminate\Database\Eloquent\Model;

class AlumnoModel extends Model{
    //nombre de la tabla
    protected $table = 'estudiante';

    //llave primaria
    protected $primarykey = 'id';
    public $timestamps = false;

    //aqui los elementos a mostrarse en la tabla 
    protected $filltable = ['id','nombre','apellido_paterno','apellido_materno','edad','direccion','telefono','bandera'];
}