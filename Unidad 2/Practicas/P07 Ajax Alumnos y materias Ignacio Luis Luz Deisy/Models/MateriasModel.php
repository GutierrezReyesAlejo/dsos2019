<?php
namespace App\Http\Models\AJAX;
use Illuminate\Database\Eloquent\Model;

class MateriasModel extends Model{
    //nombre de la tabla
    protected $table = 'materia';

    //llave primaria
    protected $primarykey = 'id';
    public $timestamps = false;

    //aqui los elementos a mostrarse en la tabla 
    protected $filltable = ['id','semestre','materia'];
}