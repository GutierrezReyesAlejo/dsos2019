<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<script src="js/jquery.min.js"></script>
<script src="js/cargar_materias.js"></script>


<body>
    {!!Form::open(array('url' => 'insertarAjax', 'method' => 'POST','autocomplete' => 'off'))!!}

    <table>
            <tr>
                <td>
                    {!!Form::label('NOMBRE: ')!!}
                    <br>
                    {!!Form::text('nombre',null)!!}
                </td>
            </tr>
            <tr>
                <td>
                    {{ Form::select('idsemestre',$sem,null,
                    ['id' =>'select-semestre','placeholder' => 'SELECCIONE'])
                    }}
                </td>
            </tr>

            <tr>
                <td>
                    <select name="idmateria" id="select-materia">
                        <option value="">SELECCIONE</option>
                    </select>
                </td>
            </tr>
            
    </table>
    {!!Form::submit('Registrar',['content'=>'<span>Registar</span>'])!!}

    {!!Form::close()!!}
    
</body>
</html>