$(function () {

    $('#select-semestre').on('change', metodo_listar);
 
 });

function metodo_listar()
{
    var materia = document.getElementById("select-semestre").value;

    $.get('lista_materias/'+materia+'', function (data){
        var html_select = '<option value="">SELECCIONE</option>';
        for (var i = 0; i < data.length; i++)
          html_select += '<option value="'+data[i].id+'">'+data[i].materia+'</option>'
   
          $('#select-materia').html(html_select);
   
      });
}