<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login','PW_Controller\Mi_Controlador@index');

//práctica 2
Route::get('visualizar','PW_Controller\estudiante_controlador@ver_datos');

Route::get('formulario','PW_Controller\estudiante_controlador@ver_formulario'); 
Route::post('insertar','PW_Controller\estudiante_controlador@insertar');//POST porque es formulario

//práctica 3
Route::get('ver_tienda','PW_Controller\tienda_controlador@ver_datos');
//GET obtener información del servidor
Route::get('formulario_t','PW_Controller\tienda_controlador@ver_formulario');
Route::post('insertar_t','PW_Controller\tienda_controlador@insertar_t'); //POST porque es formulario
//POST envia información desde el cliente para ser procesada y actualice o agregue información en el servidor

//practica 4 update
Route::get('update_t/{id}','PW_Controller\tienda_controlador@updateView');
Route::post('03Update','PW_Controller\tienda_controlador@update');

Route::post('insertar_a','PW_Controller\AlumnoController@inserta');
Route::get('alumno_formulario','PW_Controller\AlumnoController@insertView');

Route::get('alumno_eliminar_view/{nombre}','PW_Controller\AlumnoController@eliminarView');
Route::put('alumno_eliminar/{nombre}','PW_Controller\AlumnoController@eliminarDatos');

Route::get('alumno_eliminar_view_band/{nombre}','PW_Controller\AlumnoController@eliminarViewBand');
Route::put('alumno_eliminar_band/{nombre}','PW_Controller\AlumnoController@eliminarDatosBand');

Route::get('tabla','PW_Controller\AlumnoController\AlumnoControllerA@ver_tabla');

///AQUI ESTÁ FERRETERIA

Route::get('visualizar_ferre','PW_Controller\FerreteriaController@verDato');
Route::get('formulario_ferre','PW_Controller\FerreteriaController@insertView');
Route::post('insertar_ferre','PW_Controller\FerreteriaController@insertar_ferre');
Route::get('update_ferre/{id}','PW_Controller\FerreteriaController@edit_datos_ferre');
Route::post('03Update_ferre','PW_Controller\FerreteriaController@actualizar_datos_ferre');
//
Route::get('ferre_eliminar/{nombre}','PW_Controller\FerreteriaController@eliminarView_ferre');
Route::put('ferreteriaEliminar/{nombre}','PW_Controller\FerreteriaController@eliminarDatos_ferre');
//
Route::get('ferre_eliminar_view_band/{nombre}','PW_Controller\FerreteriaController@eliminarViewBandFe');
Route::put('ferreteriaEliminarBand/{nombre}','PW_Controller\FerreteriaController@eliminarDatosBandFe');

Route::get('tablaFerre','PW_Controller\FerreteriaController@ver_tablaFe');


// A J A X 
//L I S T A D O   A L U M N O S  
Route::get('lista_alumnos/{genero}','AJAX\AjaxControllers@listado_alumnos');
Route::get('ajax','AJAX\AjaxControllers@formu');

//L I S T A D O   M A T E R I A S 
Route::get('lista_materias/{materia}','AJAX\AjaxControllers@listado_materias');
Route::get('ajaxMate','AJAX\AjaxControllers@formuMateria');
Route::post('insertarAjax','Ajax\AjaxControllers@inserta');

Route::get('/', function () {
    return view('welcome');
});


///T I E N D A   A B A R R O T E S

Route::get('formulario_tienda','Ajax\AjaxControllers@ver_formulario'); 
Route::post('insertar_tienda','Ajax\AjaxControllers@insertar_t');

Route::get('formulario_productos','Ajax\AjaxControllers@formulario_productos'); 
Route::post('insertar_productos','Ajax\AjaxControllers@insertar_p');

//ajax
Route::get('lista_productos/{materia}','AJAX\AjaxControllers@listado_productos');//solo jala un producto

Route::get('ajaxProductos','AJAX\AjaxControllers@formuProductos');
Route::get('lista_productos_full','AJAX\AjaxControllers@listado_productos_full');
Route::post('insertar_productos','AJAX\AjaxControllers@insertar_producto');

//editar empresa 
Route::get('update_tienda/{id}','AJAX\AjaxControllers@edit_datos_tienda');
Route::post('tienda_update','AJAX\AjaxControllers@actualizar_datos_tienda');

Route::get('ajaxProductosv','AJAX\AjaxControllers@formuProductos');

?>