-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: dsos
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumno_materia`
--

DROP TABLE IF EXISTS `alumno_materia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumno_materia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_alumno` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `numero_control` int(11) DEFAULT NULL,
  `semestre` int(11) DEFAULT NULL,
  `materia` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno_materia`
--

LOCK TABLES `alumno_materia` WRITE;
/*!40000 ALTER TABLE `alumno_materia` DISABLE KEYS */;
INSERT INTO `alumno_materia` VALUES (1,'ALEJO GUTIERREZ',15161306,5,'16');
/*!40000 ALTER TABLE `alumno_materia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alumnos`
--

DROP TABLE IF EXISTS `alumnos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumnos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_completo` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sexo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumnos`
--

LOCK TABLES `alumnos` WRITE;
/*!40000 ALTER TABLE `alumnos` DISABLE KEYS */;
/*!40000 ALTER TABLE `alumnos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escuela`
--

DROP TABLE IF EXISTS `escuela`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `escuela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rfc` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `curp` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `num_ctrl` varchar(8) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cal1` decimal(3,1) DEFAULT NULL,
  `cal2` decimal(3,1) DEFAULT NULL,
  `cal3` decimal(3,1) DEFAULT NULL,
  `promedio` decimal(3,1) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `avalaible` tinyint(1) DEFAULT NULL,
  `deleted_at` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escuela`
--

LOCK TABLES `escuela` WRITE;
/*!40000 ALTER TABLE `escuela` DISABLE KEYS */;
INSERT INTO `escuela` VALUES (2,'carlosekjhghjk','OILOOOOOOOOOOOOOO','95839223',10.0,6.8,7.8,8.3,'2019-02-20',0,1),(3,'8g9d75a5a53j5j','carlangas','15161306',9.9,9.7,7.8,8.3,'2019-02-20',0,1),(5,'Oloberhas sisirve','ljsdljdsljdsjsd','7272828',10.0,10.0,10.0,NULL,'2019-02-07',1,1),(6,'GURA960505HOCTYL07','GURA960505HOCTYL07','7183838',7.0,8.0,5.0,NULL,'2019-03-05',NULL,NULL),(7,'HWJEWJ090923J','90S09SD09KJ32','98298289',28.0,77.0,77.0,NULL,'1992-07-29',1,NULL);
/*!40000 ALTER TABLE `escuela` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ferreteria`
--

DROP TABLE IF EXISTS `ferreteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ferreteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razon_social` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `giro` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `domicilio_fiscal` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `rfc` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `anio_ingreso` date DEFAULT NULL,
  `borrado_el` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ferreteria`
--

LOCK TABLES `ferreteria` WRITE;
/*!40000 ALTER TABLE `ferreteria` DISABLE KEYS */;
INSERT INTO `ferreteria` VALUES (2,'Comex','Pintura','No se c bro','898JJK32JK32J','Michoacan','2025-03-19',1),(3,'Cinemex','Movies','Plaza bella','090JK3KJ32KJ','Guadalajara','2016-07-20',1),(5,'POLLOS','POLLOS','NO C','73289KJ3JK','MEXICO','2025-09-26',1),(6,'Super Pitico','Super Mercado','Santa Rosa Panzacola','789DSGGFY787','Oaxaca','1988-11-12',1),(7,'Bodega Aurrera','Super Mercado','Carretera Internacional Km 54','NHU7897IHKJKJ7','Mexico','1980-12-09',0);
/*!40000 ALTER TABLE `ferreteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materia`
--

DROP TABLE IF EXISTS `materia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `semestre` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materia`
--

LOCK TABLES `materia` WRITE;
/*!40000 ALTER TABLE `materia` DISABLE KEYS */;
INSERT INTO `materia` VALUES (1,'POO',2),(2,'DSOS',8),(3,'PLF',7),(4,'DISOR',9),(5,'REDES DE COMPUTADORAS',5),(6,'FUND DE INVESTIGACION',1),(7,'ESTRUCTURAS DE DATOS',3),(8,'TOPICOS AVANZADOS DE POO',4),(9,'PROGRAMACION WEB',6),(10,'GRAFICACION',6),(11,'FUND DE PROGRAMACION',1),(12,'MATEMATICAS DISCRETAS',2),(13,'CALCULO VECTORIAL',3),(14,'TALLER DE BD',4),(15,'SIMULACION',5),(16,'ARQUITECTURA DE COMPUTADORAS',5),(17,'TICS',6),(18,'TALLER 1 DE INVESTIGACION',7),(19,'ADMIN DE REDES',8),(20,'TALLER II DE INVESTIGACION',8);
/*!40000 ALTER TABLE `materia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practica2`
--

DROP TABLE IF EXISTS `practica2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practica2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8_spanish_ci NOT NULL,
  `apellido_paterno` text COLLATE utf8_spanish_ci NOT NULL,
  `apellido_materno` text COLLATE utf8_spanish_ci NOT NULL,
  `edad` int(2) DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practica2`
--

LOCK TABLES `practica2` WRITE;
/*!40000 ALTER TABLE `practica2` DISABLE KEYS */;
INSERT INTO `practica2` VALUES (0,'Omar','Martinez','Aquino',18,'Calle Puerto Escondido','9511075508'),(1,'Alejo','Gutierrez','Reyes',22,'Andador','9511234567'),(2,'Victor Hugo','Barragan','Saucedo',22,'Privada de Venustiano Carranza','9511104710'),(3,'Lazaro','Cristobal','Sanchez',23,'Callejo Ignacio Allende','9511453407'),(5,'Gilberto','Enriquez','Domingues',23,'Juan Escutia #302','9872345893'),(7,'Gonzalo','Gonzales','Guzman',24,'Privada Niños Heroes S/N, Fracc. El Rosario','9511456792');
/*!40000 ALTER TABLE `practica2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicas`
--

DROP TABLE IF EXISTS `practicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `practicas` (
  `id` int(12) NOT NULL,
  `nombre` varchar(23) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicas`
--

LOCK TABLES `practicas` WRITE;
/*!40000 ALTER TABLE `practicas` DISABLE KEYS */;
INSERT INTO `practicas` VALUES (1,'Alejo'),(2,'Carlangas'),(3,'Juan Perez');
/*!40000 ALTER TABLE `practicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `semestre`
--

DROP TABLE IF EXISTS `semestre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `semestre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `semestre` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `semestre`
--

LOCK TABLES `semestre` WRITE;
/*!40000 ALTER TABLE `semestre` DISABLE KEYS */;
INSERT INTO `semestre` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9);
/*!40000 ALTER TABLE `semestre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tienda`
--

DROP TABLE IF EXISTS `tienda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tienda` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `razon_social` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `rfc` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_dueno` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `direccion_dueno` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_empresa` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tienda`
--

LOCK TABLES `tienda` WRITE;
/*!40000 ALTER TABLE `tienda` DISABLE KEYS */;
INSERT INTO `tienda` VALUES (1,'3232wqw','0090909','0','09kj','jjkj0i','00909','2019-02-04'),(2,'isaiodaoiasoi','iooojoioi','iooioioi','oimkkn','kkm,mm','9778778','2013-06-04'),(3,'Que onda que pez','rer2wd','juan','su ksa','no c','998939823','2015-10-06'),(4,'Pescado Frito','YN5C0CUF0','SAUL VELAZQUES','SU KASA','GANADERIA','094390439009','2012-10-09'),(5,'Mermeladas','984is98ds98','Santiago Durango','Santiago de chile','Sociedad Cooperativa','9763738282','2019-02-19');
/*!40000 ALTER TABLE `tienda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-30 12:53:22
