<?php
namespace App\Http\Models\Ajax;
use Illuminate\Database\Eloquent\Model;

class ProductoModel extends Model{
    //nombre de la tabla
    protected $table = 'producto';

    //llave primaria
    protected $primarykey = 'id';
    public $timestamps = false;

    //aqui los elementos a mostrarse en la tabla 
    protected $filltable = ['id','nombre','tipo','proveedor','precio_unitario','precio_venta'];
}
?>
