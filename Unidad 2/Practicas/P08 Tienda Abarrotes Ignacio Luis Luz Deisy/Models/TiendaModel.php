<?php
namespace App\Http\Models\Ajax;
use Illuminate\Database\Eloquent\Model;

class TiendaModel extends Model{
    //nombre de la tabla
    protected $table = 'tienda_abarrotes';

    //llave primaria
    protected $primarykey = 'id';
    public $timestamps = false;

    //aqui los elementos a mostrarse en la tabla 
    protected $filltable = ['id','rfc','razon_social','direccion_fiscal','apoderado_legal','telefono','estado'];
}
?>
