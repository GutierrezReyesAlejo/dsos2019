<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
    {!!Form::open(array('url' => 'insertar_tienda', 'method' => 'POST','autocomplete' => 'off'))!!}
    
    {!!Form::label('ID: ')!!}
    <br>
    {!!Form::text('id',null)!!}
    <br>
    {!!Form::label('RAZON SOCIAL: ')!!}
    <br>
    {!!Form::text('razon_social',null)!!}
    <br>
    {!!Form::label('RFC: ')!!}
    <br>
    {!!Form::text('rfc',null)!!}
    <br>
    {!!Form::label('Dirección fiscal: ')!!}
    <br>
    {!!Form::text('direccion_fiscal',null)!!}
    <br>
    {!!Form::label('Apoderado Legal: ')!!}
    <br>
    {!!Form::text('apoderado_legal',null)!!}
    <br>
    {!!Form::label('TELÉFONO: ')!!}
    <br>
    {!!Form::text('telefono',null)!!}
    <br>
    {!!Form::label('Estado: ')!!}
    <br>
    {!!Form::text('estado',null)!!}
    <br>
    {!!Form::submit('Registrar',['content'=>'<span>Registar</span>'])!!}

    {!!Form::close()!!}
</body>
</html>
