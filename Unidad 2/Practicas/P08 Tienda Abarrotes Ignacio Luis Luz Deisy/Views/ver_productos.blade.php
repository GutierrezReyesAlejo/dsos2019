<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<script src="../js/jquery.min.js"></script>
<script src="../js/cargar_productos.js"></script>



<body>
    {!!Form::open(array('url' => 'insertar_productos', 'method' => 'POST','autocomplete' => 'off'))!!}


    <table>
    
        <tr>
            <td>
                {!!Form::label('Nombre producto: ')!!}
            </td>
            <td>
                {{ Form::select('idproducto',$sex,null,
                ['id' =>'select-producto','placeholder' => 'SELECCIONE'])
                }}
            </td>
        </tr>
        <tr>
            <td>
                {!!Form::label('Tipo: ')!!}
            </td>
            <td>
                <select name="tipo" id="select-tipo">
                    <option value="tipo">SELECCIONE</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                {!!Form::label('Precio unitario: ')!!}
            </td>
            <td>
                {{ Form::select('idproducto',$sex,null,
                ['id' =>'select-preciou','placeholder' => 'SELECCIONE'])
                }}
            </td>
        </tr>
        <tr>
            <td>
                {!!Form::label('Precio venta: ')!!}
            </td>
            <td>
                <select name="precio_venta" id="select-preciov">
                    <option value="">SELECCIONE</option>
                </select>
            </td>
        </tr>
        
    </table>

    {!!Form::close()!!}

   
    
</body>
</html>